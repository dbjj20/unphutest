const {router, get, put, post, del, withNamespace} = require('microrouter')
const cors = require('micro-cors')()
const appPort = process.env.APP_PORT || 5000
const nameSpace = withNamespace('/api') //put `/api` for all requests
const html = require('./util/html')
const {db} = require('./db')
const moment = require('moment')
const bcrypt = require('bcrypt')
const {validateJwt, createJwtToken} = require('./db/util/validators')
const {
  createHash,
  addToStore,
  removeHash,
  doLogout
} = require('./util/storeManager')
const MicroService = require('./util/microService')
const service = new MicroService()

async function authenticate (req, res) {
  await service.createMethodNoAuth('', req, res, async function (p) {
    let { params } = p
    let hash = createHash()

    let data = await db.User.findOne({
      where: {
        email: params.email
      },
      include: [
        {model: db.UserDigest, where: { isActive: true }, limit: 1},
        {model: db.Book, include: [{model: db.Calification}]}
      ]
    })

    let user = JSON.parse(JSON.stringify(data))
    let pwd = await bcrypt.compare(
      String(params.password),
      user.userDigests[0].passwordDigest
    )

    if (pwd) {
      delete user.userDigests
      addToStore(hash)
      return {auth: true, token: await createJwtToken({hash, id: user.id}), user}
    } else {
      return {auth: false}
    }
  })
}

async function createUser (req, res) {
  await service.createMethodNoAuth('', req, res, async function (p) {
    let {params} = p
    //email, password, i wont include password confirmarion
    let {name, lastName, email, password} = params

    let user = await db.User.create({
      name,
      lastName,
      email,
      createdAt: moment().format('Y-M-D')
    })

    await db.UserDigest.create({
      passwordDigest: await bcrypt.hashSync(String(password), 10),
      userId: user.id,
      isActive: true,
      createdAt: moment().format('Y-M-D')
    })

    return user
  })
}

async function getBooks (req, res) {
  await service.createMethod('GET', req, res, async function (p) {
    return await db.Book.findAll({
      order: [['createdAt', 'DESC']],
      include: [{
        model: db.Calification
      }]
    })
  })
}

async function createBook (req, res) {
  await service.createMethod('', req, res, async function (p) {
    let {params} = p
    /* you have to send tha params bellow
      title
      description
      publicationDate
      author
      userId
    */
    return await db.Book.create(params)
  })
}

async function deleteBook (req, res) {
  await service.createMethod('GET', req, res, async function (p) {
    let {params} = p
    return await db.Book.delete(params.id)
  })
}

async function editBook (req, res) {
  await service.createMethod('', req, res, async function (p) {
    let {params} = p
    let book = await db.Book.findByPk(params.id)
    if (!book) return {error: 'no book found'}
    return await book.update({params})
  })
}

async function getBookById (req, res) {
  await service.createMethod('GET', req, res, async function (p) {
    let {params} = p
    return await db.Book.findByPk(params.id)
  })
}

async function rateBook (req, res) {
  await service.createMethod('GET', req, res, async function (p) {
    let {params} = p

    /*
      like = 1
      dislike = -1
      unlike || undislike = 0
      rate variable = String (like || unlike || dislike || undislike)
    */

    /*
    TODO: Replace logic to assign califications for the user who is making
          the request, it has to be the same in the query.

    The session logic can be improved for this.
    Any user can rate a book without their consent of any user!!.
    */

    let result = await db.Book.findOne({
      where: {
        id: params.bookId
      },
      include: [
        {model: db.Calification}
      ]
    })
    let book = JSON.parse(JSON.stringify(result))

    if (params.rate === "unlike") {
      if (book.califications[0]) {
        let rated = await db.Calification.findByPk(book.califications[0].id)
        await rated.destroy()
        return {msg: "unrated"}
      }
    }

    if (params.rate === "undislike") {
      if (book.califications[0]) {
        let rated = await db.Calification.findByPk(book.califications[0].id)
        await rated.destroy()
        return {msg: "unrated"}
      }
    }

    if (params.rate === "like") {
      if (book.califications[0]) {
        let calification = await db.Calification.findByPk(book.califications[0].id)

        return await calification.update({
          vote: 1,
          createdAt: moment().format('Y-M-D')
        })
      }else {
        return await db.Calification.create({
          vote: 1,
          userId: params.userId,
          bookId: params.bookId,
          createdAt: moment().format('Y-M-D')
        })
      }
    }

    if (params.rate === "dislike") {
      if (book.califications[0]) {
        let calification = await db.Calification.findByPk(book.califications[0].id)

        return await calification.update({
          vote: -1,
          updatedAt: moment().format('Y-M-D')
        })
      }else {
        return await db.Calification.create({
          vote: -1,
          userId: params.userId,
          bookId: params.bookId,
          createdAt: moment().format('Y-M-D')
        })
      }
    }
    return {msg: "error"}

  })
}


async function hello (req, res) {
  await service.createMethod('GET', req, res, async function(params) {
    console.log(params)
    return `${html.html}`
  })
}

const routes = cors(router(nameSpace(
    get('/books', getBooks),
    get('/book/rate/:userId/:bookId/:rate', rateBook),
    get('/book/:id', getBookById),
    del('/book/:id', deleteBook),

    post('/create/book', createBook),
    post('/create/user', createUser),
    post('/edit/book', editBook),
    post('/auth', authenticate),

    get('/', hello)
  )
))

if (process.env.NODE_ENV === 'development' || process.env.APP_MODE === 'normal') {
  module.exports = routes
} else {
  //this is for using the app in programatic mode Ex: `node app.js` (using pm2)
  const server = micro(routes)
  server.listen(appPort, () => {
    console.log(`Server listening on port ${appPort}`)
  })
}
