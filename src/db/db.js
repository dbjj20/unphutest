const getModels = require('./relations')
const {config} = require('./config')
const conf = config({env: process.env.USER})// this if custon user config to
//avoid only one user configuration

// console.log(conf)
const db = getModels(conf.dbConfig)

module.exports = db
