// allways put the new sql to the last position
module.exports = [
  `
  CREATE TABLE books
  (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title varchar(50),
    description varchar(50),
    publicationDate date,
    author varchar(50),
    userId int,
    createdAt date,
    updatedAt date
  );
  `,
  `
  CREATE TABLE califications
  (
      id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
      vote int(11),
      bookId int(11),
      userId int(11),
      createdAt date,
      updatedAt date
  );
  `,
  `
  CREATE TABLE users
  (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(50),
    lastName varchar(50),
    email varchar(50),
    createdAt date,
    updatedAt date
  );
  `,
  `
  CREATE TABLE userDigests
  (
      id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
      passwordDigest varchar(80),
      userId int,
      createdAt date
  );
  `,
  `
  CREATE UNIQUE INDEX users_email_uindex ON users (email);
  `,
  `
  ALTER TABLE userDigests ADD isActive boolean NULL;
  `,

  `ALTER TABLE califications MODIFY vote tinyint;`
]
