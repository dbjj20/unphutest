const {db} = require('../../../db')
const chalk = require('chalk')
const bcrypt = require('bcrypt')
const moment = require('moment')
const users = [
  {
    name: 'Vin',
    lastName: 'Diesel',
    email: 'Vin@email.com'
  },
  {
    name: 'Will',
    lastName: 'Smith',
    email: 'Will@email.com'
  },
  {
    name: 'Robert',
    lastName: 'Downey Jr.',
    email: 'Robert@email.com'
  },
  {
    name: 'Tom',
    lastName: 'Cruise',
    email: 'Tom@email.com'
  },
  {
    name: 'Dwayne',
    lastName: 'Johnson',
    email: 'Dwayne@email.com'
  },
  {
    name: 'Adam',
    lastName: 'Sandler',
    email: 'Adam@email.com'
  }
]
const books = [
  {
    title: 'Cien años de Soledad',
    description: 'Lorem ipsum dolor sit amet',
    author: 'Gabriel García Márquez'
  },
  {
    title: 'Don Quijote de la Mancha',
    description: 'Lorem ipsum dolor sit amet',
    author: 'Miguel de Cervantes'
  },
  {
    title: 'Hamlet',
    description: 'Lorem ipsum dolor sit amet',
    author: 'William Shakespeare',
  },
  {
    title: 'Orgullo y prejuicio',
    description: 'Lorem ipsum dolor sit amet',
    author: 'Jane Austen'
  },
  {
    title: '1984',
    description: 'Lorem ipsum dolor sit amet',
    author: 'George Orwell'
  }
]

async function createUsers () {
  try {
    for (let user of users) {
      let data = await db.User.create(user)
      let userData = JSON.parse(JSON.stringify(data))
      await db.UserDigest.create({
        passwordDigest: await bcrypt.hashSync(String("12345678"), 10),
        userId: userData.id,
        isActive: true,
        createdAt: moment().format('Y-M-D')
      })
    }
  } catch (e) {
    console.log(e)
  }
}

async function createBooks () {
  try {
    let data = await db.User.findAll()
    let users = JSON.parse(JSON.stringify(data))
    for (let book of books) {
      for (let user of users){
        await db.Book.create({
          title: book.title,
          description:  book.description,
          author: book.author,
          userId: user.id,
          createdAt: moment(`2019-10-${Math.floor((Math.random() * 20) + 1)}`).format('Y-M-D')
        })
      }
    }
  } catch (e) {
    console.log(chalk.red("Error =====>"), e)
  }
}

async function rateBooks () {
  try {
    let data = await db.User.findAll({
      include:[{ model: db.Book }]
    })
    let users = JSON.parse(JSON.stringify(data))

    for (let user of users) {
      let a = 0
      switch (Math.floor((Math.random() * 2) + 1)) {
        case 1:
          a = 1
          break
        case 2:
          a = -1
          break
        default:
          a = 0
      }
      for (let book of user.books) {
        await db.Calification.create({
          vote: a,
          bookId: book.id,
          userId: book.userId
        })
      }
    }
  } catch (e) {
    console.log(e)
  }
}

async function setup() {
    try {
      await createUsers()
      await createBooks()
      await rateBooks()
      console.log(chalk.green('Done!, Your database now have data'))
      process.exit(0)
    } catch (e) {
      console.log(e.message)
      process.exit(1)
    }
}

setup()
