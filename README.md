# UnphuTest App
For this app i'll be using `mysql`

### Installing dependencies

`npm i`

### Configuring the data base
* First you need to have your mysql db `CREATE DATABASE unphutest;` Etc.
* Then configure your db with your credentials

> File route `/src/db/config.js`

```javascript

if (options.env === 'otherUser') { // write whoami in your terminal and put you username
      config.dbConfig = {
        username: 'unphutest', //then your credentials
        password: '12345678',
        database: 'unphutest',
        host: 'localhost',
        port: '3306',
        dialect: 'mysql'
      }
    }
```

* Create the db, run `npm run create:db`
* Seeds the data base `npm run seed`

### The API

#### Run the app
1. `npm run dev` for development
2. `npm start` production
3.  Or using pm2 `pm2 start src/app` production

* First create a user
 - do `POST` to `/api/create/user`
 ```javascript
    {
	"email": "juan@email.com",
	"name": "juan",
	"lastName": "castillo",
	"password": "12345678"
    }
```

* Then you need to log in and save a token returned with the user data
 - do `POST` to `/api/auth`
```javascript
    {
	"email": "juan@email.com",
	"password": "12345678"
    }
```

* Use always the token returned for all the others requests and put it in the headers
-- header `Authorization` : `token hash` -- you dont need to put the word `Bearer` It's a `JWT` token

* Do `GET` to `/api/books` it will return all books
* Take a book id and like or unlike (+1 || -1) and Do `GET` to `/book/rate/:userId/:bookId/:rate` rate is as bellow
```javascript 
String('like'||'dislike'||'unlike'||'undislike')
```

* Do `POST` to `/create/book` and send
```javascript
    title
    description
    publicationDate
    author
    userId
```

I think i'm done for this test for now :D 🎉🎊
