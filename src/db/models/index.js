const books = require('./books')
const califications = require('./califications')
const users = require('./users')
const userDigests = require('./userDigests')

module.exports = {
  books,
  califications,
  users,
  userDigests
}
