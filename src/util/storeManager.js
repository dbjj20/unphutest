const keygen = require('keygen')
let store = []

function findSessionHash(value) {
    //fix: it should return the index, this is for removeHash
    //and stop when a value is found
    for (let a of store) {if (a === value)return {auth: true, found: a}}
}

function createHash() {
  return keygen.hex(12)
}

function addToStore(value) {
  return store.splice(0, 0, value)
}

function removeHash(index) {
  store.splice(index, 1)
}

function doLogout(value) {
  for (let i; store.length > i; i++) {
    if (store[i] === value) {
      return removeHash(store, i)
    }
  }
}

module.exports = {
  findSessionHash,
  createHash,
  addToStore,
  removeHash,
  doLogout
}

//TODO: add more complex session confirmation to avoid
// loging more than one time without logout
